﻿Imports System.Net

Public Class Form1
    '========================================
    '
    'Code by PoetralesanA
    'Subscribe Channel Youtube : BrokecoderZ
    'Link : https://youtube.com/@brokecoderz
    '
    '========================================
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        WebBrowser1.ScriptErrorsSuppressed = True
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ' Mendefinisikan URL yang akan diakses oleh WebBrowser.
        ' Dalam hal ini, URL adalah versi dasar Facebook.
        Dim url As String = "https://mbasic.facebook.com/settings/security_login"

        ' Mendefinisikan UserAgent yang akan digunakan oleh WebBrowser.
        ' Dalam hal ini, UserAgent disetel untuk meniru iPhone yang menjalankan iOS 13.2.3 dan
        ' menggunakan browser Safari versi 13.0.3.
        Dim userAgent As String = "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1"

        ' Mengubah UserAgent untuk sesi web saat ini menggunakan modul UserAgentChanger.
        ' Ini akan mempengaruhi semua permintaan web yang dibuat setelah ini.
        UserAgentChanger.SetUserAgent(userAgent)

        ' Membuat kontrol WebBrowser menavigasi ke URL yang ditentukan dengan UserAgent yang baru.
        ' Harap dicatat bahwa meskipun UserAgent telah diubah untuk sesi web
        WebBrowser1.Navigate(url, Nothing, Nothing, "User-Agent: " & userAgent)

    End Sub
End Class
Module UserAgentChanger
    <Runtime.InteropServices.DllImport("urlmon.dll", CharSet:=Runtime.InteropServices.CharSet.Ansi)>
    Private Function UrlMkSetSessionOption(
        ByVal dwOption As Integer,
        ByVal pBuffer As String,
        ByVal dwBufferLength As Integer,
        ByVal dwReserved As Integer) As Integer
    End Function

    Const URLMON_OPTION_USERAGENT As Integer = &H10000001
    Const URLMON_OPTION_USERAGENT_REFRESH As Integer = &H10000002

    Public Sub SetUserAgent(ByVal UserAgent As String)
        UrlMkSetSessionOption(URLMON_OPTION_USERAGENT_REFRESH, vbNullString, 0, 0)
        UrlMkSetSessionOption(URLMON_OPTION_USERAGENT, UserAgent, UserAgent.Length, 0)
    End Sub
End Module
'UrlMkSetSessionOption: Fungsi ini diimpor dari urlmon.dll, 
'yang merupakan bagian dari Microsoft URL Moniker Services. 
'Fungsi ini digunakan untuk mengatur opsi sesi.



'URLMON_OPTION_USERAGENT dan URLMON_OPTION_USERAGENT_REFRESH: 
'Ini adalah konstanta yang digunakan untuk menentukan opsi 
'yang akan diubah oleh UrlMkSetSessionOption. 
'URLMON_OPTION_USERAGENT digunakan untuk mengatur UserAgent, 
'dan URLMON_OPTION_USERAGENT_REFRESH digunakan untuk menyegarkan UserAgent.



'SetUserAgent: Sub ini digunakan untuk mengubah UserAgent. 
'Pertama, ia memanggil UrlMkSetSessionOption dengan URLMON_OPTION_USERAGENT_REFRESH untuk 
'menyegarkan UserAgent saat ini. Kemudian, ia memanggil UrlMkSetSessionOption lagi 
'dengan URLMON_OPTION_USERAGENT dan UserAgent baru untuk mengatur UserAgent.